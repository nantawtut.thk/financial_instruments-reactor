package com.example.financialinstruments.commands;

import com.example.financialinstruments.model.StatisticItem;
import java.util.concurrent.atomic.AtomicReference;
import lombok.Getter;
import lombok.Setter;

public class AverageCommand implements CalculatorCommand<Double> {

  private final AtomicReference<AverageMetricsData> averageProcessingAtomicReference = new AtomicReference<>(new AverageMetricsData());

  @Override
  public void execute(StatisticItem statisticItem) {
    averageProcessingAtomicReference.getAndUpdate(x -> x.increaseSum(statisticItem.getPrice()));
  }

  @Override
  public Double result() {
    return averageProcessingAtomicReference.get().average();
  }

  @Override
  public String name() {
    return "Подсчет среднего значения";
  }

  @Getter
  @Setter
  private static class AverageMetricsData {

    private double sum = 0;
    private int count = 0;

    public AverageMetricsData increaseSum(double value) {
      sum += value;
      count++;
      return this;
    }

    public Double average() {
      return count == 0 ? Double.NaN : sum / count;
    }
  }
}
