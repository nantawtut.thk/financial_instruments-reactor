package com.example.financialinstruments.commands;

import com.example.financialinstruments.model.StatisticItem;

public interface CalculatorCommand<T> {

  void execute(StatisticItem statisticItem);

  T result();

  String name();
}
