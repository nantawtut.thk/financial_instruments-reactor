package com.example.financialinstruments.commands;

import com.example.financialinstruments.model.StatisticItem;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.concurrent.atomic.AtomicReference;

public class SumOfNewestCommand implements CalculatorCommand<Double> {

  private final AtomicReference<SumOfNewestData> sumOfNewestData;

  public SumOfNewestCommand(int size) {
    sumOfNewestData = new AtomicReference<>(new SumOfNewestData(size));
  }


  @Override
  public void execute(StatisticItem statisticItem) {
    sumOfNewestData.getAndUpdate(x -> x.addIfNeeded(statisticItem));
  }

  @Override
  public Double result() {
    return sumOfNewestData.get().sum();
  }

  @Override
  public String name() {
    return String.format("Сумма последних %s значений", sumOfNewestData.get().sizeLimit);
  }

  private static class SumOfNewestData {

    private final PriorityQueue<StatisticItem> priorityQueue;
    private final int sizeLimit;

    public SumOfNewestData(int size) {
      this.priorityQueue = new PriorityQueue<>(size + 1, Comparator.comparing(StatisticItem::getDate));
      this.sizeLimit = size;
    }

    public synchronized SumOfNewestData addIfNeeded(StatisticItem statisticItem) {
      priorityQueue.add(statisticItem);
      if (priorityQueue.size() > sizeLimit) {
        priorityQueue.poll();
      }
      return this;
    }

    public Double sum() {
      Double res = 0D;
      for (StatisticItem statisticItem : priorityQueue) {
        res += statisticItem.getPrice();
      }
      return res;
    }
  }
}
