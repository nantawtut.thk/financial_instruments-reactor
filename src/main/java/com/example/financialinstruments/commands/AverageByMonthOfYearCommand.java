package com.example.financialinstruments.commands;

import com.example.financialinstruments.model.StatisticItem;

public class AverageByMonthOfYearCommand extends AverageCommand {

  private final int month;
  private final int year;

  public AverageByMonthOfYearCommand(int month, int year) {
    this.month = month;
    this.year = year;
  }

  @Override
  public void execute(StatisticItem statisticItem) {
    var date = statisticItem.getDate();
    if (date.getYear() == year && date.getMonth().getValue() == month) {
      super.execute(statisticItem);
    }
  }

  @Override
  public String name() {
    return String.format("Подсчет среднего значения за месяц %s в году %s", month, year);
  }
}
