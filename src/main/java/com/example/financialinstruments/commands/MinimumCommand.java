package com.example.financialinstruments.commands;

import com.example.financialinstruments.model.StatisticItem;
import java.util.concurrent.atomic.AtomicReference;

public class MinimumCommand implements CalculatorCommand<Double> {

  private final AtomicReference<StatisticItem> min = new AtomicReference<>();

  @Override
  public void execute(StatisticItem statisticItem) {
    min.getAndAccumulate(statisticItem, (prev, next) -> (prev == null || prev.getPrice() > next.getPrice()) ? next : prev);
  }

  @Override
  public Double result() {
    return min.get() == null ? Double.NaN : min.get().getPrice();
  }

  @Override
  public String name() {
    return "Поиск минимального значения";
  }
}
