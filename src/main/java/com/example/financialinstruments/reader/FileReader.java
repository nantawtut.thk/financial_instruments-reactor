package com.example.financialinstruments.reader;

import java.io.File;
import reactor.core.publisher.Flux;

public interface FileReader<T> {

  Flux<String> readFile(File file);
}
