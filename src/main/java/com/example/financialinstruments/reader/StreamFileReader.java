package com.example.financialinstruments.reader;

import java.io.File;
import java.nio.file.Files;
import java.util.stream.Stream;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class StreamFileReader {

  @SneakyThrows
  public Stream<String> readFile(File file) {
    return Files.lines(file.toPath());
  }
}
