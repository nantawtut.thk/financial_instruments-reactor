package com.example.financialinstruments.reader;

import java.io.File;
import java.nio.file.Files;
import java.util.stream.BaseStream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Slf4j
@Service
public class FluxFileReader {

  public Flux<String> readFile(File file) {
    return Flux.using(() -> Files.lines(file.toPath()), Flux::fromStream, BaseStream::close);
  }
}
