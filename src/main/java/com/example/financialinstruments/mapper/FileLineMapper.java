package com.example.financialinstruments.mapper;

import com.example.financialinstruments.model.StatisticItem;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class FileLineMapper {

  private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MMM-yyyy", Locale.ENGLISH);

  public static StatisticItem parseStatisticItem(String fileLine) {
    String[] lineItems = fileLine.split(",");
    return new StatisticItem(lineItems[0], LocalDate.parse(lineItems[1], DATE_TIME_FORMATTER), Double.valueOf(lineItems[2]));
  }
}
