package com.example.financialinstruments.processors;

import com.example.financialinstruments.commands.AverageByMonthOfYearCommand;
import com.example.financialinstruments.commands.AverageCommand;
import com.example.financialinstruments.commands.CalculatorCommand;
import com.example.financialinstruments.commands.MinimumCommand;
import com.example.financialinstruments.model.StatisticItem;
import com.example.financialinstruments.model.StatisticResult;
import com.example.financialinstruments.model.StatisticType;
import java.util.Collection;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class InstrumentsProcessor {

  private final CommandRegistry commandRegistry;

  public InstrumentsProcessor() {
    this.commandRegistry = new CommandRegistry();
    commandRegistry.add(StatisticType.INSTRUMENT1.name(), new AverageCommand());
    commandRegistry.add(StatisticType.INSTRUMENT2.name(), new AverageByMonthOfYearCommand(11, 2014));
    commandRegistry.add(StatisticType.INSTRUMENT3.name(), new MinimumCommand());
  }

  public Collection<CalculatorCommand<?>> handlers(String statisticName) {
    return commandRegistry.commandsByStatisticName(statisticName);
  }

  public void handle(StatisticItem statisticItem) {
    handlers(statisticItem.getName()).forEach(command -> command.execute(statisticItem));
  }


  public Collection<StatisticResult> results() {
    return commandRegistry.statisticResults();
  }
}
