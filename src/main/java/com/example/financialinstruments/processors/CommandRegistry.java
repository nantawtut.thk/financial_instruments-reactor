package com.example.financialinstruments.processors;

import com.example.financialinstruments.commands.CalculatorCommand;
import com.example.financialinstruments.commands.SumOfNewestCommand;
import com.example.financialinstruments.model.StatisticResult;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CommandRegistry {

  private final Map<String, List<CalculatorCommand<?>>> registryCommands = new HashMap<>();

  public void add(String statisticName, CalculatorCommand<?>... command) {
    var commandsByKey = registryCommands.getOrDefault(statisticName, new ArrayList<>());
    commandsByKey.addAll(Arrays.asList(command));
    registryCommands.put(statisticName, commandsByKey);
  }

  public Collection<CalculatorCommand<?>> commandsByStatisticName(String statisticName) {
    var commandFromRegistry = registryCommands.get(statisticName);
    if (commandFromRegistry != null) {
      return commandFromRegistry;
    }

    List<CalculatorCommand<?>> defaultCommands = List.of(new SumOfNewestCommand(10));
    registryCommands.put(statisticName, defaultCommands);
    return defaultCommands;
  }

  public List<StatisticResult> statisticResults() {
    return registryCommands.entrySet().stream()
        .flatMap(x -> x.getValue().stream().map(y -> new StatisticResult(x.getKey(), y.name(), y.result().toString())))
        .collect(Collectors.toList());
  }
}
