package com.example.financialinstruments.processors;

import com.example.financialinstruments.model.StatisticItem;
import com.example.financialinstruments.model.StatisticResult;
import java.io.File;
import java.util.Collection;

public interface FileProcessor<T extends StatisticItem> {

  Collection<StatisticResult> process(File file);
}
