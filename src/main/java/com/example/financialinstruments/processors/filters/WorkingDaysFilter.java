package com.example.financialinstruments.processors.filters;

import com.example.financialinstruments.model.StatisticItem;

public class WorkingDaysFilter implements Filter<StatisticItem> {

  @Override
  public Boolean test(StatisticItem item) {
    var dayOfWeek = item.getDate().getDayOfWeek().getValue();
    return (dayOfWeek != 6) && (dayOfWeek != 7);
  }
}
