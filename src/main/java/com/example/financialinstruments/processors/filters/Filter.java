package com.example.financialinstruments.processors.filters;

public interface Filter<T> {

  Boolean test(T item);
}
