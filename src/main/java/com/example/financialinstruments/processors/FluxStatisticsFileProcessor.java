package com.example.financialinstruments.processors;

import com.example.financialinstruments.mapper.FileLineMapper;
import com.example.financialinstruments.model.StatisticItem;
import com.example.financialinstruments.model.StatisticResult;
import com.example.financialinstruments.processors.filters.Filter;
import com.example.financialinstruments.reader.FluxFileReader;
import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.function.Predicate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(name = "file.processor", havingValue = "flux")
public class FluxStatisticsFileProcessor implements FileProcessor<StatisticItem> {

  private final Scheduler PROCESSING_SCHEDULER = Schedulers.fromExecutorService(Executors.newFixedThreadPool(10));
  private final Scheduler READ_SCHEDULER = Schedulers.newSingle("reader");

  private final List<Filter<StatisticItem>> filterProcessors;

  private final FluxFileReader fluxFileReader;

  @Value("${file.buffer.size}")
  private Integer bufferSize;

  @Override
  public Collection<StatisticResult> process(File file) {
    InstrumentsProcessor instrumentsProcessor = new InstrumentsProcessor();
    Flux<String> fileFlux = fluxFileReader.readFile(file);
    Predicate<StatisticItem> globalItemFilter = filterProcessors.stream()
        .map(x -> (Predicate<StatisticItem>) x::test)
        .reduce(Predicate::and)
        .orElse((item) -> true);

    return fileFlux
        .subscribeOn(READ_SCHEDULER)
        .buffer(bufferSize)
        .flatMap(x -> Flux.fromIterable(x)
            .parallel()
            .runOn(PROCESSING_SCHEDULER)
            .map(FileLineMapper::parseStatisticItem)
            .filter(globalItemFilter)
            .doOnNext(instrumentsProcessor::handle)
            .doOnError(error -> log.error("Error processing item", error))
            .sequential()
        )
        .then(Mono.defer(() -> Mono.just(instrumentsProcessor.results())))
        .block();
  }
}
