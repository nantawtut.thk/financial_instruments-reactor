package com.example.financialinstruments.processors;

import com.example.financialinstruments.mapper.FileLineMapper;
import com.example.financialinstruments.model.StatisticItem;
import com.example.financialinstruments.model.StatisticResult;
import com.example.financialinstruments.processors.filters.Filter;
import com.example.financialinstruments.reader.StreamFileReader;
import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(name = "file.processor", havingValue = "stream")
public class StreamStatisticFileProcessor implements FileProcessor<StatisticItem> {

  private final StreamFileReader streamFileReader;

  private final List<Filter<StatisticItem>> filterProcessors;

  @Value("${file.buffer.size}")
  private Integer bufferSize;

  @Override
  public Collection<StatisticResult> process(File file) {
    InstrumentsProcessor instrumentsProcessor = new InstrumentsProcessor();
    final AtomicInteger counter = new AtomicInteger(0);
    Predicate<StatisticItem> globalItemFilter = filterProcessors.stream()
        .map(x -> (Predicate<StatisticItem>) x::test)
        .reduce(Predicate::and)
        .orElse((item) -> true);
    Stream<String> fileStream = streamFileReader.readFile(file);

    fileStream
        .collect(Collectors.groupingBy(it -> counter.getAndIncrement() / bufferSize))
        .values()
        .forEach(x -> x.parallelStream()
            .map(FileLineMapper::parseStatisticItem)
            .filter(globalItemFilter)
            .forEach(instrumentsProcessor::handle));
    return instrumentsProcessor.results();
  }
}
