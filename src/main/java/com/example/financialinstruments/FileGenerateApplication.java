package com.example.financialinstruments;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Random;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileGenerateApplication {

  private static String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

  public static void main(String[] args) {
    run(args);
  }

  private static void run(String... args) {
    try {
      if (args.length < 1) {
        throw new RuntimeException("Bad count of input params. Needed 1 [filePath]");
      }
      for (String filePath : args) {
        Random random = new Random();
        try (OutputStream outputStream = new FileOutputStream(filePath)) {
          long rowCount = 1000 * 1000;
          String line;
          for (long i = 0; i < rowCount; i++) {
            var statistic = statisticName(random);
            line = String.format("%s,%s,%s", statistic, statisticDate(random, statistic), random.nextInt(100) + random.nextDouble());
            log.info("insert line {}: {} file {}", i, line, filePath);
            outputStream.write((i < rowCount - 1 ? line + "\n" : line).getBytes());
          }
        }
      }
    } catch (Exception e) {
      log.error("Error while filling statistic file", e);
      System.exit(1);
    }
  }

  private static String statisticName(Random random) {
    String[] stringList = {"INSTRUMENT1", "INSTRUMENT2", "INSTRUMENT3", "INSTRUMENT_CUSTOM1"};
    return stringList[random.nextInt(stringList.length)];
  }

  private static String statisticDate(Random random, String statistic) {
    if (statistic.equals("INSTRUMENT2") && random.nextInt(100) > 70) {
      return String.format("%s-Nov-2014", day(random));
    } else {

      return String.format("%s-%s-%s", day(random), months[random.nextInt(12)], 2000 + (2020 - 2010 + random.nextInt(11)));
    }
  }

  private static String day(Random random) {
    int day = 1 + random.nextInt(28);
    return day < 10 ? "0" + day : Integer.valueOf(day).toString();
  }
}
