package com.example.financialinstruments;

import com.example.financialinstruments.model.StatisticItem;
import com.example.financialinstruments.processors.FileProcessor;
import java.io.File;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StopWatch;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class FinancialInstrumentsApplication implements CommandLineRunner {

  private final FileProcessor<StatisticItem> processor;

  public static void main(String[] args) {
    SpringApplication.run(FinancialInstrumentsApplication.class, args);
  }

  @Override
  public void run(String... args) {
    try {
      if (args.length < 1) {
        throw new RuntimeException("Bad count of input params. Needed 1 [filePath]");
      }

      log.info("Processor file: {}", processor.getClass());

      long fileMs = 0;

      for (String filePath : args) {
        long ms = 0;
        int iter = 50;
        log.info("***************************");
        log.info("******* NEW FILE *********");
        log.info(filePath);
        log.info("***************************");
        var result1 = processor.process(new File(filePath));
        System.out.println(result1.size());
        for (int i = 0; i < iter; i++) {
          StopWatch stopWatch = new StopWatch();
          stopWatch.start();
          var result = processor.process(new File(filePath));
          stopWatch.stop();
          //          for (StatisticResult statisticResult : result) {
          //            log.info(statisticResult.toString());
          //          }
          ms += stopWatch.getLastTaskTimeMillis();
          log.warn("took {}ms file {}", stopWatch.getLastTaskTimeMillis(), filePath);
        }
        var average = ms / iter;
        fileMs += average;
        log.warn("average time execution {}ms for file {}", average, filePath);
      }

      log.warn("average time execution {}ms for all files", fileMs);
      System.exit(1);
    } catch (Exception e) {
      log.error("Error while calculating statistic", e);
      System.exit(1);
    }
  }
}
