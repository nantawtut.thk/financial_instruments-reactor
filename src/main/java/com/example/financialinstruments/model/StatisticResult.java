package com.example.financialinstruments.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StatisticResult {

  private String statisticName;
  private String commandName;
  private String result;

  @Override
  public String toString() {
    return String.format("Statistic: %s \t Command: %s \t Result: %s", statisticName, commandName, result);
  }
}
