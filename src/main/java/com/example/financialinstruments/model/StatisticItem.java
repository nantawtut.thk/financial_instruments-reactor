package com.example.financialinstruments.model;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StatisticItem {

  private String name;
  private LocalDate date;
  private Double price;

  public String getName() {
    return name.toUpperCase();
  }
}
